import java.util.*;
import org.xml.sax.*;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.File;

public class XMLCounter implements ContentHandler {

	/* here comes var declaration */
    ArrayList<Integer> value;
    ArrayList<String> pathTree;
    String elementText;
    String deweyID;
    String pathName;
    Map<String, PrintWriter> openPaths;

	public void startDocument( ) throws SAXException {

     /* here comes the init code */
        // Just for efficiency, the arraylist has the initial capacity
        // of 30 elements
        value    = new ArrayList<>(30);
        pathTree = new ArrayList<>(30);
        openPaths= new HashMap<>();
        value.add(1);
    }
    PrintWriter openPath(String type) throws IOException{
        new File(pathName + "/").mkdirs();

        String fullPath = pathName + "/" + type;
        if (!openPaths.containsKey(fullPath))
        {
            PrintWriter retVal = new PrintWriter(fullPath, "UTF-8");
            openPaths.put(fullPath, retVal);
            return retVal;
        }
        return openPaths.get(fullPath);
    }

	public void startElement(String uri, String localName, String qName, 
	            Attributes attributes) throws SAXException {

        deweyID = value.toString();
        deweyID = deweyID.substring(1, deweyID.length() -1).replace(", ", ".");
        pathTree.add(qName);
        pathName = pathTree.toString();
        pathName = pathName.substring(1, pathName.length() -1).replace(", ", "/");

        try{
            PrintWriter writer = openPath("tag");
            writer.println(deweyID + " " + qName);
        } catch (IOException e) { }

        try{
            PrintWriter writer = openPath("attr");
            for (int i = 0; i < attributes.getLength(); ++i) {
                writer.println(deweyID + " " + attributes.getQName(i)
                                       + " " + attributes.getValue(i));
            }
        } catch (IOException e) { }

        value.add(1);
        
	}

    public void endElement(String uri, String localName,
                           String qName) throws SAXException {
        

        value.remove(value.size()-1);
        value.set(value.size()-1,value.get(value.size()-1) + 1);

        pathTree.remove(pathTree.size()-1);
    }
    
    public void characters(char ch[], int start, int length) throws SAXException {
        
        /* here comes code after the startElement is met */

        elementText = new String(ch, start, length).trim();
        if (elementText.length() != 0) {
            try{
                PrintWriter writer = openPath("text");
                writer.println(deweyID + " " + elementText);
            } catch (IOException e) { }
        }
		
    }
    
    
    public void endDocument( ) throws SAXException {
        for (PrintWriter writer : openPaths.values()) {
            writer.close();
        }
    }
    
    // Do-nothing methods we have to implement only to fulfill
    // the interface requirements:
    
    public void ignorableWhitespace(char ch[], int start, int length) throws SAXException {}
    
    public void processingInstruction(String target, String data) 	   
    throws SAXException {}
    public void setDocumentLocator(Locator locator) {}
    public void startPrefixMapping(String prefix, String uri)
    throws SAXException {}
    public void endPrefixMapping(String prefix) throws SAXException {}
    public void skippedEntity(String name) throws SAXException {}
    
}
