import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class JoinReducer extends Reducer<TextPair, Text, Text, Text> {

  @Override
  protected void reduce(TextPair key, Iterable<Text> values, Context context)
      throws IOException, InterruptedException {

	boolean found = false;
	for (Text t : values) {
		if(key.getSecond().toString().equals(JoinMapper1.class.getName()))
			found = true;
		else if (found)
			context.write(new Text(t.toString()), new Text(""));
	}
  }
  
}
