#!/bin/bash
firstPath="sbml/model/listOfReactions/reaction/listOfProducts/speciesReference/attr"
secondPath="sbml/model/listOfReactions/reaction/listOfReactants/speciesReference/attr"

attrName="species"
attrValue="P_KK"

# First query
    #1: filter the attrName
    #2: filter the attrData
    #3: filter everything beyond the first four numbers
    #4: add a \ before each . to allow grep in next query
for line in $(cat $firstPath | sed -e "s/ /#\$#/g" \
          | sed -e /\[^\#\]*\#\\\$\#$attrName/\!d \
                -e /\[^\#\]*\#\\\$\#\[^\#\]*\#\\\$\#\.*$attrValue\.*/\!d \
                -e "s/\(^[^.]*\.[^.]*\.[^.]*\.[^.]*\.\).*/\1/" \
                -e "s/\./\\\\./g" \
          | uniq)
do
    # do the next query and check if it exists in the first query
	cat $secondPath | grep $line
done | sort

