import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class JoinMapper2 extends Mapper<LongWritable, Text, TextPair, Text> {
    @Override
	protected void map(LongWritable key, Text value, Context context)
	throws IOException, InterruptedException {
  
	String[] values = value.toString().split(" ");

	String deweyID = values[0];
	String attrName= values[1];
	String attrData= values[2];

	if (attrName.equals("species")) {
	    String[] deweyIdentifiers = deweyID.split("\\.");
	    int removeLength = deweyID.length();
	    if (deweyIdentifiers.length > 2) {
		removeLength = deweyIdentifiers[deweyIdentifiers.length - 2].length() 
					+ deweyIdentifiers[deweyIdentifiers.length - 1].length() + 2;
	    }

	    String deweyPid = deweyID.substring(0, deweyID.length() - removeLength);
	    //TextPair pair = new TextPair(deweyID, attrData);
	    TextPair pair = new TextPair(deweyPid, this.getClass().getName());
	    context.write(pair, new Text(deweyID + " " + attrData));
	}
    }
}
