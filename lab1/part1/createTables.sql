SELECT 'Dropping all tables';
drop table subTopics;
drop table topicExperts;
drop table topics;
drop table recommendation;
drop table experts;

SELECT 'Creating table for experts';
CREATE TABLE experts(
    id int identity(1,1) not null ,
    email varchar(255) not null,
    name varchar(255) not null,
    description varchar(255) not null,

    PRIMARY KEY (id)
);


SELECT 'Creating table for topics';
CREATE TABLE topics(
    id int identity(1,1) not null,
    originator int not null,
    text varchar(255) not null,

    PRIMARY KEY(id),
    FOREIGN KEY(originator) REFERENCES experts(id)
);


SELECT 'Creating table for subTopics';
CREATE TABLE subTopics(
    topic int not null,
    subTopic int not null,

    CONSTRAINT pk_subTopics PRIMARY KEY(topic,subTopic),
    FOREIGN KEY(topic) REFERENCES topics(id),
    FOREIGN KEY(subTopic) REFERENCES topics(id)
);


SELECT 'Creating table for recommendation';
CREATE TABLE recommendation(
    id int identity(1,1) not null,
    writer int not null,
    recommends int not null,
    text varchar(255) not null,

    PRIMARY KEY(id),
    FOREIGN KEY(writer) REFERENCES experts(id),
    FOREIGN KEY(recommends) REFERENCES experts(id)
);

CREATE TABLE topicExperts(
    expert_id int not null,
    topic_id int not null,

    CONSTRAINT pk_topicExperts PRIMARY KEY(expert_id,topic_id),
    FOREIGN KEY(expert_id) REFERENCES experts(id),
    FOREIGN KEY(topic_id) REFERENCES topics(id),
);
