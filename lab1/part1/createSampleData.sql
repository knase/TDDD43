INSERT INTO experts(email, name, description)
VALUES
('apa@expert.not', 'Apa Banansson', 'Loves bananas'),
('giraff@expert.not', 'Giraff Longneck', 'Sees very far'),
('fox@expert.not', 'Fox Bunnyeater', 'What does the fox say'),
('queen@expert.not', 'Ad Queen', 'Like to ride my bicycle'),
('king@expert.not', 'Martin Luther', 'Not racist'),
('skywalker@expert.not', 'A Skywalker', 'Secretly your father'),
('itcrowd@expert.not', 'Nerd Nerdsson', 'Did you try turning it off and on again'),
('ned@expert.not', 'N Flanders', 'Secret Atheist')

INSERT INTO topics(originator, text)
VALUES
(8, 'godly'),
(7, 'Computer Science'),
(6, 'Databases'),
(5, 'SQL')

INSERT INTO topicExperts(expert_id, topic_id)
VALUES
(6,1),
(1,2),
(3,3),
(2,1),
(1,3),
(1,4)

INSERT INTO recommendation(text, writer, recommends)
VALUES
('Like!', 1,3),
('My King kong', 1,5),
('Did you try getting off and on again?', 7,4),
('Bless you', 8,4),
('Mmm tasty', 3,1)

INSERT INTO subTopics(topic, subTopic)
VALUES
(1,2),
(2,3),
(3,4)