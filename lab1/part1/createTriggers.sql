CREATE TRIGGER trigNewExpertise ON topics AFTER INSERT
AS
SET NOCOUNT ON

    INSERT INTO topicExperts(expert_id, topic_id)
    SELECT originator, id FROM INSERTED
GO
