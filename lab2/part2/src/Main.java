import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class Main {

	public static void main(String[] args) {
		// Parse the OWL file, and create the model for the ontology
		OntModel m = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, null);

		try {
			// Open the file existing in part one, this
			// is one level up, so get the current path of where we are.
			String currentPath = new java.io.File("").getAbsolutePath();
			m.read("file://"+currentPath + "/../part1/university.owl");
		}
		catch (com.hp.hpl.jena.shared.WrappedIOException e) {
			if (e.getCause() instanceof java.io.FileNotFoundException) {
				System.err.println("A java.io.FileNotFoundException caught: " 
						+ e.getCause().getMessage());
				System.err.println("You must alter the path passed to " +
						"OntModel.read() so it finds your university " +
						"ontology");
			}
		}
		catch (Throwable t) {
			System.err.println("Caught exception, message: " + t.getMessage());
		}

		new ClassHierarchy().showHierarchy(System.out, m);
	}
}
