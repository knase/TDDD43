
package fr.inrialpes.exmo.align.impl.method; 

import java.util.Properties;
import java.lang.reflect.Method;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.AlignmentException;

import fr.inrialpes.exmo.align.impl.MatrixMeasure;

import java.util.ArrayList;

public class RSDistAlignment extends StringDistAlignment implements AlignmentProcess {

    Method dissimilarity = null;
    String methodName = "rsDistance";


    protected ArrayList<String> nGrams(String str, int grams) {
        ArrayList<String> retVal = new ArrayList<>();


        // Creates all the substrings containing 1 to <grams>
        // characters
        for (int g = 1; g <= grams; ++g) {
            for (int i = 0; i <= str.length() - g; ++i) {
                retVal.add(str.substring(i, g+i));
            }
        }

        return retVal;
    }
    /** Creation **/ 
    public RSDistAlignment() {
        setSimilarity( new MatrixMeasure() {
            public double measure( Object o1, Object o2 ) throws Exception {
                // Get the two strings to compare
                String s1 = ontology1().getEntityName( o1 );
                String s2 = ontology2().getEntityName( o2 );
                
                // Return 1.0 (max distance) if either of the two entities are null
                if ( s1 == null || s2 == null ) {
                    return 1.;
                }

                // Return 0.0 (no distance) if the two strings are
                // equal
                if (s1.toLowerCase().equals(s2.toLowerCase())) {
                    return 0.;
                }

                ArrayList<String> s1hash = nGrams(s1.toLowerCase(), 3);
                ArrayList<String> s2hash = nGrams(s2.toLowerCase(), 3);



                int totalLengthA = 0;
                for (String s : s1hash) {
                    if (s2hash.contains(s)) {
                        totalLengthA += s.length();
                    }
                	
                }
                double distance = 1.0 - ((double)totalLengthA / ( ((double)s1.length() + (double)s2.length()) * (double)2.0) );

                // abs value of distance
                return (distance < -distance ? 0 : distance);
                
            }

            public double classMeasure( Object cl1, Object cl2 ) throws Exception {
                return measure( cl1, cl2 );
            }

            public double propertyMeasure( Object pr1, Object pr2 ) throws Exception{
                return measure( pr1, pr2 );
            }

            public double individualMeasure( Object id1, Object id2 ) throws Exception{
                return measure( id1, id2 );
            }
        });
        setType("**");
    }

    /* Processing */
    public void align( Alignment alignment, Properties params ) throws AlignmentException {

        // Initialize matrix
        getSimilarity().initialize( ontology1(), ontology2(), alignment );

        // Compute similarity/dissimilarity
        getSimilarity().compute( params );

        // Print matrix if asked
        if ( params.getProperty("printMatrix") != null ) {
            printDistanceMatrix( params );
        }

        // Extract alignment
        extract( type, params );
    }

}



